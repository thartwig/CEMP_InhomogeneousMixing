### creating Fig. 4 ###
### run: gnuplot Figure4.gnu ###

set terminal postscript enhanced eps color size 6cm,6cm "Times-Roman, 16"

set key top right box opaque
set key samplen 3
unset key
set yrange [ 0 : 5.5 ]
set xrange [ 0 : 250 ]


set style fill noborder
set grid front
unset grid

set out "Figure4.eps"

set ylabel '{/Symbol D}[C/H]' offset 1
set xlabel 'pc'

set label '0% of CEMP-no stars' at 100,0.4 font ",16" tc rgb "red" front
set label '100% of CEMP-no stars' at 5,5.2 font ",16" tc rgb "dark-green" front


plot \
'Yoon_logD_dCoH_percentiles.dat' u 1:4:14 w filledcurve lc rgb "grey90" ti '',\
'Yoon_logD_dCoH_percentiles.dat' u 1:5:13 w filledcurve lc rgb "grey80" ti '',\
'Yoon_logD_dCoH_percentiles.dat' u 1:6:12 w filledcurve lc rgb "grey70" ti '',\
'Yoon_logD_dCoH_percentiles.dat' u 1:7:11 w filledcurve lc rgb "grey60" ti '',\
'Yoon_logD_dCoH_percentiles.dat' u 1:8:10 w filledcurve lc rgb "grey50" ti '',\
'Yoon_logD_dCoH_percentiles.dat' u 1:4 w filledcurve y1=0 lc rgb "pink" ti '0%',\
'Yoon_logD_dCoH_percentiles.dat' u 1:9 w l lw 5 lc -1 ti '50%',\
'Yoon_logD_dCoH_percentiles.dat' u 1:14 w filledcurve y1=6 lc rgb "light-green" ti '100%'