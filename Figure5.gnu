### creating Fig. 5 ###
### run: gnuplot Figure5.gnu ###
### you need to manually insert blank lines between each data block: Yoon_D_dCoH_3.dat -> Yoon_D_dCoH_3_lines.dat ###

set terminal postscript enhanced eps color size 6cm,8.0cm "Times-Roman, 16"

set key above vertical maxrows 1 font ",14"
set key samplen 2
ymax = 3.95
set yrange [ 0 : ymax ]
set xrange [ -5.5 : -3 ]
set format x ''
unset key

set out "Figure5.eps"
set multiplot


set tmargin at screen 0.82
set bmargin at screen 0.58
set lmargin at screen 0.2
set rmargin at screen 0.97

set pm3d interpolate 1,1
set pm3d map
set cbrange [0:4]
set palette maxcolors 8
set palette defined (0 "green", 1 "yellow", 3 "orange", 4 "red")
set colorbox horiz user origin .2,.92 size .77,.04

splot 'Yoon_D_dCoH_3_lines.dat' u 1:2:3
unset pm3d

set format y ''
plot 'CEMP-no-YoonIDs.dat' lc -1

set pm3d interpolate 1,1
set pm3d map
set format y '%g'
set ylabel '[C/Fe]'

unset colorbox
set tmargin at screen 0.58
set bmargin at screen 0.34
unset key


splot 'Yoon_D_dCoH_3_lines.dat' u 1:2:4

unset pm3d
set format y ''
unset ylabel
plot 'CEMP-no-YoonIDs.dat' lc -1
plot 1.85 dt 3 lw 4 lc rgb "grey50"
set pm3d interpolate 1,1
set pm3d map
set format y '%g'

unset colorbox
set xlabel '[Fe/H]' offset 0,0.5
set format x "%g"

set tmargin at screen 0.34
set bmargin at screen 0.10



splot 'Yoon_D_dCoH_3_lines.dat' u 1:2:5

set label 'd=10pc' at -5.3,3.5+ymax+ymax font ",14"
set label 'd=30pc' at -5.3,3.5+ymax font ",14"
set label 'd=100pc' at -5.3,3.5 front font ",14"
set label '{/Symbol D} [C/H]' at screen 0.04,0.945

set label '22%' at -5.3,1.1+ymax+ymax tc rgb "dark-green"
set label '8%' at -5.3,1.1+ymax tc rgb "dark-green"
set label '0%' at -5.3,1.1 front tc rgb "dark-green"
set label 'Group II, Yoon+16' at -4.85,1.4+ymax front tc rgb "grey50" font ",12"
set arrow 2 from -4.9,ymax+1.85 to -4.9,ymax+1.34 lc rgb "grey50"
set arrow 4 from -3.9,ymax+1.85 to -3.9,ymax+1.34 lc rgb "grey50"

unset pm3d
set format y ''
set format x ''
set xlabel ''
unset ylabel
plot 'CEMP-no-YoonIDs.dat' lc -1
set pm3d interpolate 1,1
set pm3d map
set format y '%g'
set ylabel '[C/Fe]'