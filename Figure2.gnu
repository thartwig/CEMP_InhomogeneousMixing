### creating Fig. 2 ###
### run: gnuplot Figure2.gnu ###

set terminal postscript enhanced eps color size 6cm,6cm "Times-Roman, 16"

set key top right
set key samplen 3
set logscale y
set yrange [2e5:2e9]
set ylabel "t_{coll}/yr" offset -1
set format y "10^{%L}"
set xrange [ -6 : -1 ]
set format x ''

set out "Figure2.eps"
set multiplot

set tmargin at screen 0.97
set bmargin at screen 0.55
set lmargin at screen 0.2
set rmargin at screen 0.96

#change initial conditions in Mathematica accordingly to obtain necessary data files

plot \
"CoH_vs_Tcoll_n1000_T100_d30.dat" w l title "100K" lc 1 dt 2 lw 4,\
"CoH_vs_Tcoll_n1000_T200_d30.dat" w l title "200K" lc -1 dt 1 lw 4,\
"CoH_vs_Tcoll_n1000_T400_d30.dat" w l title "400K" lc 2 dt 3 lw 4


set xlabel '[C/H]' offset 0,0.5
set format x "%g"

set tmargin at screen 0.55
set bmargin at screen 0.13


set label 'n=10^3cm^{-3}' at -5.7,1e10 font ",14"
set label 'T=200K' at -5.7,1e6 font ",14"


plot \
"CoH_vs_Tcoll_n100_T200_d30.dat" w l title "10^2cm^{-3}" lc 1 dt 2 lw 4,\
"CoH_vs_Tcoll_n1000_T200_d30.dat" w l title "10^3cm^{-3}" lc -1 dt 1 lw 4,\
"CoH_vs_Tcoll_n10000_T200_d30.dat" w l title "10^4cm^{-3}" lc 2 dt 3 lw 4