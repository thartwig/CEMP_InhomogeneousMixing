### creating Fig. 3 ###
### run: gnuplot Figure3.gnu ###

set terminal postscript enhanced eps color size 6cm,4cm "Times-Roman, 16"

set key top left
set key samplen 2
set yrange [ 0 : 2.5 ]
set xrange [ -5 : -1 ]

set out "Figure3.eps"

set ylabel '{/Symbol D}[C/H]'#offset 1
set xlabel '[C/H]'


plot \
"CoH_vs_dCoH_n1000_T200_d100.dat" w l title "d=100pc" lc 2 dt 3 lw 4,\
"CoH_vs_dCoH_n1000_T200_d30.dat" w l title "d=30pc" lc -1 dt 1 lw 4,\
"CoH_vs_dCoH_n1000_T200_d10.dat" w l title "d=10pc" lc 1 dt 2 lw 4