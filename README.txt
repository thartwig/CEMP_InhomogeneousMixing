*** Formation of carbon-enhanced metal-poor stars as a consequence of inhomogeneous metal mixing ***

################################
########## 2018/12/12 ##########
##########    v1.2    ##########
################################

This folder contains a Mathematica notebook (*.nb), plotting scripts (*.gnu), and observational data from Jinmi Yoon (*.dat) to reproduce the results presented in Hartwig & Yoshida 2018:
arXiv:1812.01820
2019ApJ...870L...3H
DOI: 10.3847/2041-8213/aaf866

The notebook and script contain basic information on their usage. Please refer to the paper for more details.

All data are free to use, modify, and share if you refer to the original publication.


*** Contact ***

If you have questions, discover problems or inconsistencies, please contact: hartwig@phys.s.u-tokyo.ac.jp
